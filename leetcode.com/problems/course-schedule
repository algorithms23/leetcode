/*
https://leetcode.com/problems/course-schedule/

207. Course Schedule

There are a total of numCourses courses you have to take, labeled from 0 to numCourses - 1. You are given an array prerequisites where prerequisites[i] = [ai, bi] indicates that you must take course bi first if you want to take course ai.

For example, the pair [0, 1], indicates that to take course 0 you have to first take course 1.
Return true if you can finish all courses. Otherwise, return false.

Example 1:

Input: numCourses = 2, prerequisites = [[1,0]]
Output: true
Explanation: There are a total of 2 courses to take. 
To take course 1 you should have finished course 0. So it is possible.

Example 2:

Input: numCourses = 2, prerequisites = [[1,0],[0,1]]
Output: false
Explanation: There are a total of 2 courses to take. 
To take course 1 you should have finished course 0, and to take course 0 you should also have finished course 1. So it is impossible.

*/

class Solution {
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        Map<Integer, ArrayList<Integer>> graph = new HashMap<>();
        Map<Integer, Integer> indegree = new HashMap<>();

        for (int i = 0; i < numCourses; i++) {
            graph.put(i, new ArrayList<>());
            indegree.put(i,0);
        }

        for(int[] prerequisite : prerequisites) {
            int parent = prerequisite[0];
            int child = prerequisite[1];

            ArrayList<Integer> pList = graph.get(parent);
            pList.add(child);

            graph.put(parent, pList);
            indegree.put(child, indegree.get(child) + 1);
        }

        Deque<Integer> q = new ArrayDeque<>();
        for (Map.Entry<Integer, Integer> e : indegree.entrySet()) {
            if (e.getValue() == 0) {
                q.addLast(e.getKey());
            }
        }

        List<Integer> finishedCourse = new ArrayList<>();

        while(!q.isEmpty()){
            Integer node = q.poll();
            finishedCourse.add(node);
            for(int i = 0; i < graph.get(node).size(); ++i){
                int val = graph.get(node).get(i);
                indegree.put(val, indegree.get(val) - 1);
                if(indegree.get(val) == 0){
                    q.addLast(val);
                }
            }
        }
        return finishedCourse.size() == numCourses;

    }
}

