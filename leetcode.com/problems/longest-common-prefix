/*
https://leetcode.com/problems/longest-common-prefix/
14. Longest Common Prefix
Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".

Example 1:

Input: strs = ["flower","flow","flight"]
Output: "fl"

Example 2:

Input: strs = ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.

*/

class Solution {
    public String longestCommonPrefix(String[] strs) {
    
        String mainString = strs[0];
        int ind=0;
        
        for (int c=0;c<mainString.length();c++)
        {
            ind++;
            char chr = mainString.charAt(c);
            
            for (int i=1;i<strs.length;i++)
            {
                if (c>=strs[i].length()) 
                    return mainString.substring(0,c);
                if (chr !=  strs[i].charAt(c))
                {
                    return mainString.substring(0,c);
                }
            }
        }
        
        return mainString.substring(0,ind);
    }
}

// USING TRIE

class Solution {
    public String longestCommonPrefix(String[] strs) {
        
        Node root = new Node();

        for (int i = 0; i < strs.length; i++) { // n
            Node cur = root;
            for (char c : strs[i].toCharArray()) { // m
                if (!cur.children.containsKey(c)) {
                    cur.children.put(c, new Node());
                }
                cur = cur.children.get(c);
                cur.prefixCount++;
            }
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < strs[0].length(); i++) { // m
            
            if (root.children.containsKey(strs[0].charAt(i)))
            {
                if ( root.children.get(strs[0].charAt(i)).prefixCount == strs.length) {
                    root = root.children.get(strs[0].charAt(i));
                    sb.append(strs[0].charAt(i));
                }
            }
            else 
                break;
        }
        return sb.toString();
    }

    class Node {
        HashMap<Character, Node> children = new HashMap<>();
        int prefixCount;

        public Node() {
            children = new HashMap<>();
            prefixCount = 0;
        }
    }
}
