/*
https://leetcode.com/problems/find-in-mountain-array/

1095. Find in Mountain Array

(This problem is an interactive problem.)

You may recall that an array arr is a mountain array if and only if:

arr.length >= 3
There exists some i with 0 < i < arr.length - 1 such that:
arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
arr[i] > arr[i + 1] > ... > arr[arr.length - 1]
Given a mountain array mountainArr, return the minimum index such that mountainArr.get(index) == target. If such an index does not exist, return -1.

You cannot access the mountain array directly. You may only access the array using a MountainArray interface:

MountainArray.get(k) returns the element of the array at index k (0-indexed).
MountainArray.length() returns the length of the array.
Submissions making more than 100 calls to MountainArray.get will be judged Wrong Answer. Also, any solutions that attempt to circumvent the judge will result in disqualification.

Example 1:

Input: array = [1,2,3,4,5,3,1], target = 3
Output: 2
Explanation: 3 exists in the array, at index=2 and index=5. Return the minimum index, which is 2.
Example 2:

Input: array = [0,1,2,4,2,1], target = 3
Output: -1
Explanation: 3 does not exist in the array, so we return -1.
*/

/**
 * // This is MountainArray's API interface.
 * // You should not implement it, or speculate about its implementation
 * interface MountainArray {
 *     public int get(int index) {}
 *     public int length() {}
 * }
 */
 
class Solution {
    public int findInMountainArray(int target, MountainArray arr) {
        int peak = findPeak(arr);
        int minIndex;
        minIndex = binarySearch(target, arr, 0, peak, true);
        return minIndex == -1 ? binarySearch(target, arr, peak + 1, arr.length() - 1, false) : minIndex;
        
    }

    private int findPeak(MountainArray arr) {
        int left = 0;
        int right = arr.length() - 1;
        int mid;
        
        while (left < right) {
            mid = left + ((right - left) / 2);
            if (arr.get(mid) > arr.get(mid + 1)) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        
        return left;
    }

    private int binarySearch(int target, MountainArray arr, int left, int right, boolean isAscending) {
        
        int mid;
        int midElement;
        
        while (left <= right) {
            mid = left + ((right - left) / 2);
            midElement = arr.get(mid);
            if ( midElement == target) {
                return mid;
            } 
            if (isAscending) {
                if (midElement < target) {
                    left = mid + 1;
                } else {
                    right = mid - 1;
                }
            } else {
                if (midElement < target) {
                    right = mid - 1;
                } else {
                    left = mid + 1;
                }
            }
        }
        
        return -1;
    }
}
