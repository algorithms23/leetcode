/*
https://leetcode.com/problems/letter-case-permutation

784. Letter Case Permutation

Given a string s, you can transform every letter individually to be lowercase or uppercase to create another string.

Return a list of all possible strings we could create. Return the output in any order.

Example 1:

Input: s = "a1b2"
Output: ["a1b2","a1B2","A1b2","A1B2"]
Example 2:

Input: s = "3z4"
Output: ["3z4","3Z4"]
*/

class Solution {

    List<String> result = new ArrayList<>();
    String str;

    public List<String> letterCasePermutation(String s) {
        str = s;
        backtrack(new StringBuilder(), 0);
        return result;
    }

    private void backtrack(StringBuilder sb, int index) {
        if (index == str.length()) {
            result.add(sb.toString());
            return;
        }
        char c = str.charAt(index);
        if (Character.isDigit(c)) {
            sb.append(c);
            backtrack(new StringBuilder(sb), index + 1);
        } else {
            sb.append(Character.toLowerCase(c));
            backtrack(new StringBuilder(sb), index + 1);
            sb.deleteCharAt(sb.length() - 1);
            sb.append(Character.toUpperCase(c));
            backtrack(new StringBuilder(sb), index + 1);
            sb.deleteCharAt(sb.length() - 1);
        }
    }
}
