/*
https://leetcode.com/problems/least-number-of-unique-integers-after-k-removals/

1481. Least Number of Unique Integers after K Removals

Given an array of integers arr and an integer k. Find the least number of unique integers after removing exactly k elements.

Example 1:

Input: arr = [5,5,4], k = 1
Output: 1
Explanation: Remove the single 4, only 5 is left.
Example 2:
Input: arr = [4,3,1,1,3,3,2], k = 3
Output: 2
Explanation: Remove 4, 2 and either one of the two 1s or three 3s. 1 and 3 will be left.

*/

class Solution {
    public int findLeastNumOfUniqueInts(int[] arr, int k) {
        HashMap<Integer,Integer> map = new HashMap<>();
        PriorityQueue<Integer> pq = new PriorityQueue<>((a,b) -> Integer.compare(map.get(a),map.get(b)));
        
        for (int i = 0; i < arr.length; i++) {
            map.put(arr[i], map.getOrDefault(arr[i],0) + 1);
        }
        
        for (int i : map.keySet()) {
            pq.add(i);
        }
        
        while(k > 0) {
            int val = pq.peek();
                        
            while (map.containsKey(val) && k > 0) {
                k--;
                if (map.get(val) == 1) {
                    map.remove(val);
                    pq.poll();
                } else {
                    map.put(val,map.get(val) - 1);
                }
            }
            
        }
        
        return map.size();
        
    }
}
