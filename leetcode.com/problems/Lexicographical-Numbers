/*
https://leetcode.com/problems/Lexicographical-Numbers/

386. Lexicographical Numbers

Given an integer n, return all the numbers in the range [1, n] sorted in lexicographical order.

You must write an algorithm that runs in O(n) time and uses O(1) extra space. 

Example 1:

Input: n = 13
Output: [1,10,11,12,13,2,3,4,5,6,7,8,9]

Example 2:

Input: n = 2
Output: [1,2]

*/

// USING TRIE

class Solution {
    
    Node root;

    public List<Integer> lexicalOrder(int n) {
        List<Integer> result = new ArrayList<>();

        root = new Node();

        for (int i = 1; i <= n; i++) {
            insert(i);
        }

        Node cur = root;

        dfs(result, cur);

        return result;
    }

    public void dfs(List<Integer> result, Node cur) {
        if (root != cur)
        result.add (cur.val);
        
        for (Map.Entry<Character, Node> n : cur.children.entrySet()) {
            dfs(result, n.getValue());
        }
    }
    public void insert(int num) {
        String str = "" + num;
        Node cur = root;

        for (Character c : str.toCharArray()) {
            if (!cur.children.containsKey(c)) {
                cur.children.put(c, new Node());
            }
            cur = cur.children.get(c);
        }
       cur.val = num;
    }

    class Node {
        HashMap <Character, Node>  children = new HashMap<>();
        int val;
    }

}
